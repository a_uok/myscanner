package com.example.myscanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.net.MalformedURLException;
import java.net.URL;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
public class MainActivity extends AppCompatActivity {

    Button goToScanBtn, goToGenerateBtn;
    private IntentIntegrator qrScan;
    AlertDialog.Builder alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        goToScanBtn = (Button) findViewById(R.id.goToScanBtn);
        goToGenerateBtn = (Button) findViewById(R.id.goToGenerateBtn);
        alert = new AlertDialog.Builder(this);
        requestPer();
        goToGenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, GenerateActivity.class);
                startActivity(i);
            }
        });
        goToScanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateScanner();
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.help_btn:
                showInfoDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initiateScanner() {
        qrScan = new IntentIntegrator(this);
        qrScan.setOrientationLocked(false);
        qrScan.setPrompt("Place the Camera above the QR Code to scan ..\n     Press back button to cancel");
        qrScan.initiateScan();
    }

    public void showOutText(String content)
    {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10,10,10,10);

        final TextView outTextView = new TextView(this);
        outTextView.setLayoutParams(params);
        outTextView.setText(content);
        outTextView.setTextColor(getResources().getColor(android.R.color.holo_blue_light));
        outTextView.setTextSize(20);
        alert.setMessage("The QR Code Result");
        alert.setTitle("Done");

        if(URLUtil.isValidUrl(content))
        {
            outTextView.setPaintFlags(outTextView.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
            outTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(content));
                    startActivity(browserIntent);
                }
            });
        }

        alert.setView(outTextView);
        alert.setPositiveButton("Copy", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("QR_RESULT", content);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(), "Text copied successfully!", Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
              dialog.dismiss();
            }
        });

        alert.show();
    }

    public static boolean IsValidUrl(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException ignored) {
        }
        return false;
    }
    private void showInfoDialog() {
        new AlertDialog.Builder(this)
                .setTitle("MyScanner App")
                .setMessage("Hello\nThis app is developed by Tarek Elias\n\n\nFeel free to contact with me at:\ntarek.elias97@gmail.com")
                .setNegativeButton("OK!", null)
                .setIcon(R.drawable.ic_baseline_help_outline_24)
                .show();
    }

    @AfterPermissionGranted(123)
    private void requestPer() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            Toast.makeText(this, "Permission Granted!", Toast.LENGTH_SHORT).show();
        } else {
            EasyPermissions.requestPermissions(this, "Please grant permissions to continue using the app!", 123, perms);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(@NonNull String permission) {
        return super.shouldShowRequestPermissionRationale(permission);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
        }

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                showOutText(result.getContents());
            }
        }
    }
}